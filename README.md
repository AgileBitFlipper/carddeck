# README #

This project was designed to be built and tested with JetBrains IntelliJ IDE.  However, it should be able
to be built and debugged with just about any environment.  The output of this repository is the CardDeck
JAR file that can be used with other projects.

### What is this repository for? ###

This project is designed to provide for a standard deck of 52 cards.  There are four (4) suits; Clubs, 
Diamonds, Hearts, and Spades.  There are 13 cards in each suit; A, 2, 3, 4, 5, 6, 7, 8, 9, 10, J, Q,
K.  Functionality in this deck includes shuffle, cut and draw.

### How do I get set up? ###

* Summary of set up
Clone the repository to your local system and open the project with IntelliJ.

* Configuration
Configuration should be minimal.

* Dependencies
The code depends on JDK 1.8, Groovy 2.4.12 (test), and jUnit 4.11 (test).

* Deployment instructions
Copy the $project.name JAR file to your local libraries and include it as a dependency in your project

### Contribution guidelines ###
Contact me for information on contributing to this project (instructions below).

* Writing tests
Tests are provided, and create a 100% code coverage of the library.

* Code review
This project has not been formally reviewed by others, but I would be delighted to have anyone take that job.
* Other guidelines

### Who do I talk to? ###
Feel free to contact @AgileBitFlipper on Twitter or amontcrieff@cfl.rr.com for email if there are any questions.
