package com.thirdsonsoftware;

import static org.hamcrest.Matchers.*;
import java.security.InvalidParameterException;
import static org.hamcrest.MatcherAssert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CardTest {

    private int id;

    private Face face;

    private Suit suit;

    private int value;

    private Card card;

    private final int idAceClubs = 1;
    private final int valueAceClubs = 1;

    final Card testTwoOfDiamondsCard = new Card(15, Face.TWO, Suit.DIAMONDS, Face.TWO.getValue());
    final Card testQueenOfHeartsCard = new Card(38, Face.QUEEN, Suit.HEARTS, Face.QUEEN.getValue());
    final Card testAceOfClubsCard = new Card(1, Face.ACE, Suit.CLUBS, Face.ACE.getValue());

    @Before
    public void setup() {
        face = Face.ACE;
        suit = Suit.CLUBS;
        value = valueAceClubs;
        id = idAceClubs;
        Card.setUseColor(false);
        this.card = new Card(id, face, suit, value);
        card.setInHand(false);
        card.setPlayed(false);
    }

    @Test
    public void shouldSetUseColors() {
        Card.setUseColor(false);
        assertThat(card.toString(), is("A♣( 1)- 1"));
        Card.setUseColor(true);
        assertThat(card.toString(), is("[36mA♣( 1)- 1[0m"));
    }

    @Test
    public void shouldSetId() {
        assertThat(card.getId(), is(idAceClubs));
        card.setId(3000);
        assertThat(card.getId(), is(3000));
    }

    @Test
    public void shouldGetId() {
        assertThat(card.getId(), is(valueAceClubs));
    }

    @Test
    public void shouldGetPlayed() {
        assertThat(card.getPlayed(), is(false));
    }

    @Test
    public void shouldGetInHand() {
        assertThat(card.getInHand(), is(false));
    }

    @Test
    public void shouldGetFace() {
        assertThat(card.getFace(), is(Face.ACE));
    }

    @Test
    public void shouldGetSuit() {
        assertThat(card.getSuit(), is(Suit.CLUBS));
    }

    @Test
    public void shouldGetFaceChar() {
        assertThat(card.getFaceChar(), is(Face.ACE.getCharacter()));
    }

    @Test
    public void shouldGetSuitChar() {
        assertThat(card.getSuitChar(), is('♣'));
    }

    @Test
    public void shouldGetValue() {
        assertThat(card.getValue(), is(valueAceClubs));
    }

    @Test
    public void shouldThrowOnDisplayCardWithUnallocatedLines() {
        String[] row = new String[5];
        Assertions.assertThrows(InvalidParameterException.class, () -> {
            card.displayCard(row);
        });
    }

    @Test
    public void shouldDisplayCard() {
        // Whole rows is null
        Assertions.assertThrows(InvalidParameterException.class, () -> {
            card.displayCard(null);;
        });

        // Not enough rows.
        Assertions.assertThrows(InvalidParameterException.class, () -> {
            String[] rows = new String[2];
            rows[0] = "";
            rows[1] = "";
            card.displayCard(rows);;
        });

        // Any row null.
        for (int j = 0; j < 5; j++) {
            String[] rows = new String[] {"", "", "", "", ""};
            rows[j] = null;
            // At least one row in all rows is null...
            Assertions.assertThrows(InvalidParameterException.class, () -> {
                card.displayCard(rows);;
            });
        }

        String[] rows = new String[] {"", "", "", "", ""};
        card.displayCard(rows);
        assertThat(rows.length, is(5));
        assertThat(rows[0], is(" ----- "));
        assertThat(rows[1], is("|A♣   |"));
        assertThat(rows[2], is("| ( 1)|"));
        assertThat(rows[3], is("|   ♣A|"));
        assertThat(rows[4], is(" ----- "));

        rows = new String[] {"", "", "", "", ""};
        card.setInHand(true);
        card.displayCard(rows);
        assertThat(rows.length, is(5));
        assertThat(rows[0], is(" ----- "));
        assertThat(rows[1], is("|A♣   |"));
        assertThat(rows[2], is("| ( 1)|"));
        assertThat(rows[3], is("|   ♣A|"));
        assertThat(rows[4], is(" --H-- "));

        rows = new String[] {"", "", "", "", ""};
        card.setInHand(false);
        card.setPlayed(true);
        card.displayCard(rows);
        assertThat(rows.length, is(5));
        assertThat(rows[0], is(" ----- "));
        assertThat(rows[1], is("|A♣   |"));
        assertThat(rows[2], is("| ( 1)|"));
        assertThat(rows[3], is("|   ♣A|"));
        assertThat(rows[4], is(" --P-- "));

    }

    @Test
    public void shouldCompareTo() {
        assertThat(card.compareTo(testAceOfClubsCard), is(0));
        assertThat(card.compareTo(testQueenOfHeartsCard), is(-1));
        assertThat(card.compareTo(testTwoOfDiamondsCard), is(-1));
        assertThat(testQueenOfHeartsCard.compareTo(testTwoOfDiamondsCard), is(1));
    }

    @Test
    public void shouldHashCode() {
        assertThat(card.hashCode(), is(-1149777556));
    }

    @Test
    public void shouldEquals() {
        Object o = Integer.valueOf(7);
        assertThat(card.equals(o), is(false));
        assertThat(card.equals(testAceOfClubsCard), is(true));
        assertThat(card.equals(testQueenOfHeartsCard), is(false));
        assertThat(card.equals(testTwoOfDiamondsCard), is(false));
    }

    @Test
    public void shouldToString() {
        assertThat(card.toString(), is("A♣( 1)- 1"));
        assertThat(testAceOfClubsCard.toString(), is("A♣( 1)- 1"));
        assertThat(testQueenOfHeartsCard.toString(), is("Q♥(38)-10"));
        assertThat(testTwoOfDiamondsCard.toString(), is("2♦(15)- 2"));
    }
}
