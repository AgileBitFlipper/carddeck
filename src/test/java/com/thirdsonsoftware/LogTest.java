package com.thirdsonsoftware;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.After;
import static com.github.stefanbirkner.systemlambda.SystemLambda.tapSystemOut;
import static com.github.stefanbirkner.systemlambda.SystemLambda.tapSystemErr;
import static com.github.stefanbirkner.systemlambda.SystemLambda.muteSystemOut;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class LogTest {

    public Log log ;

    @Before
    public void setup() {
        log = new Log();
    }

    @After
    public void teardown() {
        Log.setDebugMode(false);
    }

    @Test
    public void shouldGetCallerClass() {
        int level = 1;

        try {
            Class actualValue = Log.getCallerClass(level);
            assertThat(actualValue, is(this.getClass()));
        } catch (ClassNotFoundException e) {
            assertTrue(false);;
        }
    }

    @Test
    public void testSetDebugMode() {
        boolean bModeEnabled = false;
        Log.setDebugMode(bModeEnabled);
        assertThat(Log.debugMode, is(bModeEnabled));
        Log.setDebugMode(!bModeEnabled);
        assertThat(Log.debugMode, is(not(bModeEnabled)));
    }

    @Test
    public void shouldInfo() throws Exception {
        String message = "Info message.";
        String text = tapSystemOut(() -> {
            Log.Info(message);
        });
        assertThat(text.trim(), is("[37mInfo message.[0m"));
    }

    @Test
    public void shouldError() throws Exception {
        String message = "Error message.";
        String text = tapSystemErr(() -> {
            Log.Error(message);
        });
        assertThat(text.trim(), is("[31mError message.[0m"));
    }

    @Test
    public void shouldDebug() throws Exception {
        Log.setDebugMode(true);
        String message = "Debug message.";
        String text = tapSystemOut(() -> {
            Log.Debug(message);
        });
        assertThat(text.trim(), is("[34mDebug message.[0m"));
        Log.setDebugMode(false);
        muteSystemOut(() -> {
            Log.Debug(message);
        });
    }

    @Test
    public void shouldWarning() throws Exception {
        String message = "Warning message.";
        String text = tapSystemOut(() -> {
            Log.Warning(message);
        });
        assertThat(text.trim(), is("[33mWarning message.[0m"));
    }
}
