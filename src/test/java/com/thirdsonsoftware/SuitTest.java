package com.thirdsonsoftware;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class SuitTest {

    private Suit club = Suit.CLUBS;
    private Suit diamond = Suit.DIAMONDS;
    private Suit heart = Suit.HEARTS;
    private Suit spade = Suit.SPADES;

    @Before
    public void setup() {

    }

    @Test
    public void testClub() {
        assertThat(club.getSuitName(), is(Suit.CLUBS.suitName()));
    }
	@Test
	public void testDiamond() {
		assertThat(diamond.getSuitName(), is(Suit.DIAMONDS.suitName()));
	}
    @Test
	public void testHeart() {
		assertThat(heart.getSuitName(), is(Suit.HEARTS.suitName()));
	}
    @Test
	public void testSpade() {
		assertThat(spade.getSuitName(), is(Suit.SPADES.suitName()));
	}
	
    @Test
    public void testClubSymbol() {
        assertThat(club.getSuitSymbol(), is(Suit.CLUBS.symbol()));
    }
	@Test
	public void testDiamondSymbol() {
		assertThat(diamond.getSuitSymbol(), is(Suit.DIAMONDS.symbol()));
	}
    @Test
	public void testHearSymbol() {
		assertThat(heart.getSuitSymbol(), is(Suit.HEARTS.symbol()));
	}
    @Test
	public void testSpadeSymbol() {
		assertThat(spade.getSuitSymbol(), is(Suit.SPADES.symbol()));
	}
	
    @Test
    public void shouldCompile() {
        Suit heart = Suit.HEARTS;
        assertThat(heart.getSuitName(), is("Hearts"));
        assertThat(heart.getSuitSymbol(), is("\u2665"));
    }
}
