package com.thirdsonsoftware;

import static org.hamcrest.Matchers.*;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.collection.IsEmptyCollection.emptyCollectionOf;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CardDeckTest {

    private CardDeck cardDeck;
    private CardDeck clubsOnlyDeck;

    private List<Card> fullDeck = new ArrayList<Card>(52);
    private List<Card> oneSuit = new ArrayList<Card>(13);
    private List<Card> clubsOnly = new ArrayList<Card>(13);

    @Before
    public void setup() {
        this.cardDeck = new CardDeck();
        this.clubsOnlyDeck = new CardDeck();

        int id = 1;
        // Once Suite only - Clubs
        for (Face face : Face.values()) {
            oneSuit.add(new Card(id, face, Suit.CLUBS, id));
            id++;
        }
        clubsOnly = new ArrayList<>(oneSuit);
        clubsOnlyDeck.setDeck(clubsOnly);

        // All suits
        id = 1;
        for (Suit s : Suit.values()) {
            for (Face f : Face.values()) {
                fullDeck.add(new Card(id++, f, s, f.getValue()));
            }
        }
    }

    @Test
    public void shouldGetDeck() {
        assertThat(cardDeck.getDeck(), is(fullDeck));
    }

    @Test
    public void shouldSetDeck() {
        cardDeck.setDeck(oneSuit);
        assertThat(cardDeck.getDeck(), is(clubsOnly));
        assertThat(cardDeck.display(CardDeck.DISPLAY_AS_A_STRING), is(
                "Card deck (13):\n  [A♣( 1)- 1, 2♣( 2)- 2, 3♣( 3)- 3, 4♣( 4)- 4, 5♣( 5)- 5, 6♣( 6)- 6, 7♣( 7)- 7, 8♣( 8)- 8, 9♣( 9)- 9, T♣(10)-10, J♣(11)-11, Q♣(12)-12, K♣(13)-13\n   ]\n"));
    }

    @Test
    public void shouldGenerateCards() {
        cardDeck.setDeck(new ArrayList<Card>(52));
        assertThat(cardDeck.getDeck(), is(emptyCollectionOf(Card.class)));
        cardDeck.generateCards();
        assertThat(cardDeck.getDeck(), is(fullDeck));
    }

    @Test
    public void shouldCutDeck() {
        Card cutCard = clubsOnlyDeck.cutDeck();
        assertThat(cutCard, isA(Card.class));
        assertThat(cutCard.getSuit(), is(Suit.CLUBS));
    }

    @Test
    public void shouldCutDeckMultipleTimes() {
        // Cut negative times.
        Assertions.assertThrows(InvalidParameterException.class, () -> {
            clubsOnlyDeck.cutDeck(-1);
        });

        // Cut zero times.
        Assertions.assertThrows(InvalidParameterException.class, () -> {
            clubsOnlyDeck.cutDeck(0);
        });

        // Cut small deck too many times.
        Assertions.assertThrows(InvalidParameterException.class, () -> {
            clubsOnlyDeck.cutDeck(clubsOnly.size());
        });

        // Cut small deck too many times.
        Assertions.assertThrows(InvalidParameterException.class, () -> {
            clubsOnlyDeck.cutDeck(2);
        });

        List<Card> cutCards = clubsOnlyDeck.cutDeck(1);
        List<Card> uniqueCards = new ArrayList<Card>();
        for (Card card : cutCards) {
            assertThat(card, isA(Card.class));
            assertThat(card.getSuit(), is(Suit.CLUBS));
            assertThat(uniqueCards, not(hasItem(card)));
            uniqueCards.add(card);
        }

        cutCards = cardDeck.cutDeck(5);
        uniqueCards = new ArrayList<Card>();
        for (Card card : cutCards) {
            assertThat(card, isA(Card.class));
            assertThat(uniqueCards, not(hasItem(card)));
            uniqueCards.add(card);
        }
    }

    @Test
    public void shouldShuffleCardsWithNullDeck() {

        Assertions.assertThrows(NullPointerException.class, () -> {
            clubsOnlyDeck.setDeck(null);
            clubsOnlyDeck.shuffleCards();
        });
    }

    @Test
    public void shouldShuffleCards() {

        Assertions.assertThrows(InvalidParameterException.class, () -> {
            clubsOnlyDeck.shuffleCards(-1);
        });

        Assertions.assertThrows(InvalidParameterException.class, () -> {
            clubsOnlyDeck.shuffleCards(0);
        });

        List<Card> cards = new ArrayList<>(clubsOnlyDeck.getDeck());
        clubsOnlyDeck.shuffleCards();
        assertThat(clubsOnlyDeck, not(cards));

        cards = new ArrayList<>(clubsOnlyDeck.getDeck());
        clubsOnlyDeck.shuffleCards(5);
        assertThat(clubsOnlyDeck.getDeck(), not(cards));

    }

    @Test
    public void shouldDisplay() {

        // Empty deck of cards.
        CardDeck emptyDeck = new CardDeck();
        List<Card> emptyList = new ArrayList<>();
        emptyDeck.setDeck(emptyList);
        assertThat(emptyDeck.display(CardDeck.DISPLAY_AS_A_STRING), is("Card deck (0):\n  [<empty>]\n"));

        // Display deck as cards
        assertThat(clubsOnlyDeck.display(CardDeck.DISPLAY_AS_A_CARD), 
            is("Card deck (13):\n"
                + "       -----  -----  -----  -----  -----  -----  -----  -----  -----  -----  -----  -----  ----- \n"
                + "      |A♣   ||2♣   ||3♣   ||4♣   ||5♣   ||6♣   ||7♣   ||8♣   ||9♣   ||T♣   ||J♣   ||Q♣   ||K♣   |\n"
                + "      | ( 1)|| ( 2)|| ( 3)|| ( 4)|| ( 5)|| ( 6)|| ( 7)|| ( 8)|| ( 9)|| (10)|| (11)|| (12)|| (13)|\n"
                + "      |   ♣A||   ♣2||   ♣3||   ♣4||   ♣5||   ♣6||   ♣7||   ♣8||   ♣9||   ♣T||   ♣J||   ♣Q||   ♣K|\n"
                + "       -----  -----  -----  -----  -----  -----  -----  -----  -----  -----  -----  -----  ----- \n"));

        // Display deck as a string.
        assertThat(clubsOnlyDeck.display(CardDeck.DISPLAY_AS_A_STRING), 
            is("Card deck (13):\n"
            + "  [A♣( 1)- 1, 2♣( 2)- 2, 3♣( 3)- 3, 4♣( 4)- 4, 5♣( 5)- 5, 6♣( 6)- 6, 7♣( 7)- 7, 8♣( 8)- 8, 9♣( 9)- 9,"
            + " T♣(10)-10, J♣(11)-11, Q♣(12)-12, K♣(13)-13\n   ]\n"));
    }

    @Test
    public void shouldDisplayCard() {

        //String[] rows = new String[] {"", "", "", "", ""};
        Card aceOfClubs = clubsOnlyDeck.getDeck().get(0);

        // Invalid display type
        Assertions.assertThrows(InvalidParameterException.class, () -> {
            String[] rows = new String[] {"", "", "", "", ""};
            clubsOnlyDeck.displayCard(32, aceOfClubs, rows);
        });

        // invalid card
        Assertions.assertThrows(InvalidParameterException.class, () -> {
            String[] rows = new String[] {"", "", "", "", ""};
            clubsOnlyDeck.displayCard(CardDeck.DISPLAY_AS_A_CARD, null, rows);
        });

        // invalid rows argument
        Assertions.assertThrows(InvalidParameterException.class, () -> {
            clubsOnlyDeck.displayCard(CardDeck.DISPLAY_AS_A_CARD, aceOfClubs, null);
        });

        // invalid row in list of rows
        String[] rowsOfNull = new String[] {null, null, null, null, null};
        Assertions.assertThrows(InvalidParameterException.class, () -> {
            clubsOnlyDeck.displayCard(CardDeck.DISPLAY_AS_A_CARD, aceOfClubs, rowsOfNull);
        });

        // invalid row in list of rows
        String[] rowsTooFew = new String[] {"", "", ""};        
        Assertions.assertThrows(InvalidParameterException.class, () -> {
            clubsOnlyDeck.displayCard(CardDeck.DISPLAY_AS_A_CARD, aceOfClubs, rowsTooFew);
        });

        // Display a card as a string
        String[] rowsForCardString = new String[] {null};
        Assertions.assertThrows(InvalidParameterException.class, () -> {
            clubsOnlyDeck.displayCard(3, aceOfClubs, rowsForCardString);
        });

        Assertions.assertThrows(InvalidParameterException.class, () -> {
            clubsOnlyDeck.displayCard(CardDeck.DISPLAY_AS_A_STRING, aceOfClubs, rowsForCardString);
        });

        rowsForCardString[0] = "";
        clubsOnlyDeck.displayCard(CardDeck.DISPLAY_AS_A_STRING, aceOfClubs, rowsForCardString);
        assertThat(rowsForCardString[0], is("A♣( 1)- 1"));

        Assertions.assertThrows(InvalidParameterException.class, () -> {
            String[] rows = new String[] {null, "", "", "", ""};
            clubsOnlyDeck.displayCard(CardDeck.DISPLAY_AS_A_CARD, aceOfClubs, rows);
        });

        Assertions.assertThrows(InvalidParameterException.class, () -> {
            String[] rows = new String[] {"", null, "", "", ""};
            clubsOnlyDeck.displayCard(CardDeck.DISPLAY_AS_A_CARD, aceOfClubs, rows);
        });

        Assertions.assertThrows(InvalidParameterException.class, () -> {
            String[] rows = new String[] {"", "", null, "", ""};
          clubsOnlyDeck.displayCard(CardDeck.DISPLAY_AS_A_CARD, aceOfClubs, rows);         });

          
        Assertions.assertThrows(InvalidParameterException.class, () -> {
            String[] rows = new String[] {"", "", "", null, ""};
            clubsOnlyDeck.displayCard(CardDeck.DISPLAY_AS_A_CARD, aceOfClubs, rows);
        });

        
        Assertions.assertThrows(InvalidParameterException.class, () -> {
            String[] rows = new String[] {"", "", "", "", null};
            clubsOnlyDeck.displayCard(CardDeck.DISPLAY_AS_A_CARD, aceOfClubs, rows);
        });

        String[] rows = new String[] {"", "", "", "", ""};
        clubsOnlyDeck.displayCard(CardDeck.DISPLAY_AS_A_CARD, aceOfClubs, rows);
        assertThat(rows.length, is(5));
        assertThat(rows[0], is(" ----- "));
        assertThat(rows[1], is("|A♣   |"));
        assertThat(rows[2], is("| ( 1)|"));
        assertThat(rows[3], is("|   ♣A|"));
        assertThat(rows[4], is(" ----- "));
    }
}
