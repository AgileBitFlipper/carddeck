/*
 *
 * Copyright (c) 2018 Third Son Software, LLC (http://thirdsonsoftware.com/) and others. All rights
 * reserved. Created by Andrew B. Montcrieff on 1/9/18 6:34 PM
 *
 * This file is part of Project "carddeck".
 *
 * carddeck can not be copied and/or distributed without the express permission of Andrew B.
 * Montcrieff or Third Son Software, LLC.
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *
 * Last modified: 1/9/18 12:03 AM
 *
 */

package com.thirdsonsoftware;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * A class that provides for a standard deck of cards. It provides functionality like shuffle, cut,
 * and display. todo: Additional functionality will be added to draw a Card, deal a Card, and
 * topCard.
 */
public class CardDeck {

    public static final int DEFAULT_COUNT = 52; // Total number of cards in a deck
    public static final int MINIMUM_CUT = 5; // Minimum number of cards to leave before and after a
                                             // cut

    public static final int DISPLAY_AS_A_STRING = 0;
    public static final int DISPLAY_AS_A_CARD = 1;

    List<Card> deck;

    /**
     * Returns the complete deck of Cards
     * 
     * @return (List of Card) list representing a deck of Cards
     */
    public List<Card> getDeck() {
        return deck;
    }

    /**
     * Allows the deck to be set to the provided List of Cards
     * 
     * @param deck the deck of Cards to set the deck to
     */
    public void setDeck(List<Card> deck) {
        this.deck = deck;
    }

    /**
     * The main constructor for the CardDeck. This generates the standard, and complete, deck of
     * Cards; A - K, each having the suits of Clubs, Diamonds, Hearts, and Spades
     */
    public CardDeck() {
        deck = new ArrayList<Card>(DEFAULT_COUNT);
        generateCards();
    }

    /**
     * Generates the complete, standard deck of cards
     */
    protected void generateCards() {
        int id = 1;
        Card card;
        for (Suit s : Suit.values()) {
            for (Face f : Face.values()) {
                card = new Card(id++, f, s, f.getValue());
                deck.add(card);
            }
        }
    }

    /**
     * Cuts the deck of cards, providing back a single card of the cut.
     * 
     * @return Card of the cut
     */
    protected Card cutDeck() {
        List<Card> cuts = cutDeck(1);
        return cuts.get(0);
    }

    /**
     * Cuts the deck of cards multiple times, providing back a List of Cards representing each cut.
     * 
     * @param numCuts how many cuts to cut the deck
     * @return (List of Card) a List of Card with each cut
     */
    protected List<Card> cutDeck(int numCuts) {

        Random randomSeed = new Random();

        List<Card> cutCards = new ArrayList<Card>(1);

        // You can't cut too small, or have more cuts than there are cards for a minimum cut.
        if (numCuts <= 0) {
            throw new InvalidParameterException(
                    String.format("Number of cuts ({}) too small.", numCuts));
        } else if (deck.size() <= numCuts) {
            throw new InvalidParameterException(
                    String.format("Not enough cards ({}) in deck for the number of cuts ({}).",
                            deck.size(), numCuts));
        } else if (deck.size() < (MINIMUM_CUT * (numCuts + 1))) {
            throw new InvalidParameterException(String.format(
                    "Not enough cards ({}) in deck for the minimum size of each cut ({}).",
                    deck.size(), MINIMUM_CUT));
        } else {

            int numberOfCardsToCut;
            int cardIndex = MINIMUM_CUT - 1;
            int minimumNumberOfCardsToLeaveInDeckAfterCut;

            for (int cutNum = 0; cutNum < numCuts; cutNum++) {

                minimumNumberOfCardsToLeaveInDeckAfterCut = ((numCuts - cutNum) * MINIMUM_CUT) - 1;
                numberOfCardsToCut =
                        deck.size() - minimumNumberOfCardsToLeaveInDeckAfterCut - (cardIndex + 1);

                cardIndex = (cardIndex + 1) + randomSeed.nextInt(numberOfCardsToCut);

                Log.Info(String.format("  Cutting deck - Cut %d yields card '%s'.", cutNum + 1,
                        deck.get(cardIndex - 1)));

                cutCards.add(deck.get(cardIndex - 1));
            }
        }
        return cutCards;
    }

    /**
     * Shuffles the deck of cards a single time.
     */
    protected void shuffleCards() {
        shuffleCards(1);
    }

    /**
     * Shuffles the deck of cards the provided number of times.
     * 
     * @param howManyTimes (int) the number of times to cut the deck
     */
    protected void shuffleCards(int howManyTimes) {

        if (deck == null) {
            throw (new NullPointerException("Card deck has not been allocated yet."));
        } else if (howManyTimes <= 0) {
            throw (new InvalidParameterException(
                    "The number of times to shuffle needs to be greater than zero."));
        } else {
            Log.Info(String.format(" Shuffling card deck multiple(%d) times...", howManyTimes));
            for (int attempt = 0; attempt < howManyTimes; attempt++) {
                Collections.shuffle(getDeck());
            }
        }
    }

    /**
     * Provides for the display of a deck of cards in a String. The cards can be represented as a
     * String or a Card.
     * 
     * @param type specify how to display the deck (DISPLAY_AS_A_CARD, DISPLAY_AS_A_STRING)
     * @return String representing the deck of cards
     */
    protected String display(int type) {

        StringBuilder strCards = new StringBuilder();
        strCards.append(String.format("Card deck (%d):\n", getDeck().size()));
        if (getDeck().isEmpty()) {
            strCards.append("  [<empty>]\n");
        } else {
            if (type == DISPLAY_AS_A_CARD) {
                String rows[] = new String[5];
                for (int i = 0; i < 5; i++)
                    rows[i] = "      ";
                for (Card card : getDeck()) {
                    displayCard(type, card, rows);
                }
                for (String str : rows)
                    strCards.append(str + "\n");
            } else {
                strCards.append("  [");
                int c = 0;
                for (Card card : getDeck()) {
                    if (getDeck().lastIndexOf(card) != deck.size() - 1)
                        strCards.append(card).append(", ");
                    else
                        strCards.append(card);
                    if (++c % 13 == 0 && c != 52) {
                        strCards.append("\n   ");
                    }

                }
                strCards.append("]\n");
            }
        }
        return strCards.toString();
    }

    /**
     * Provides for the ability to display a single Card. This is used by methods to display a deck
     * as well.
     * 
     * @param type specify how to display the deck (DISPLAY_AS_A_CARD, DISPLAY_AS_A_STRING)
     * @param c the Card to display
     * @param row the row of Strings that will be used to define the card in
     */
    protected void displayCard(int type, Card c, String[] row) {

        if (type != DISPLAY_AS_A_CARD && type != DISPLAY_AS_A_STRING) {
            throw (new InvalidParameterException(
                    String.format("The display type provided, %d, is not valid.", type)));
        } else if (c == null) {
            throw (new InvalidParameterException("No card provided to display."));
        } else if (row == null) {
            throw (new InvalidParameterException("No string array provided to display the card."));
        } else if (type == DISPLAY_AS_A_STRING && row[0] == null) {
            throw (new InvalidParameterException(
                    "A string array with adequate space is required to display a card as a string."));
        } else if (type == DISPLAY_AS_A_CARD && row.length < 5) {
            throw (new InvalidParameterException(
                    "A string array with at least 5 elements is required to display a card."));
        } else if (type == DISPLAY_AS_A_CARD && (row[1] == null || row[2] == null
                || row[3] == null || row[4] == null)) {
            throw (new InvalidParameterException(
                    "A string array with adequate space is required to display a card as a 5-line card."));
        } else {
            switch (type) {
                case DISPLAY_AS_A_CARD:
                    c.displayCard(row);
                    break;
                case DISPLAY_AS_A_STRING:
                    row[0] = c.toString();
                    break;
            }
        }
    }
}
