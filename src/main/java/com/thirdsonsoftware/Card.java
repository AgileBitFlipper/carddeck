/*
 *
 * Copyright (c) 2018 Third Son Software, LLC
 * (http://thirdsonsoftware.com/) and others. All rights reserved.
 * Created by Andrew B. Montcrieff on 1/9/18 6:34 PM
 *
 * This file is part of Project "carddeck".
 *
 * carddeck can not be copied and/or distributed without the express
 * permission of Andrew B. Montcrieff or Third Son Software, LLC.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Last modified: 1/8/18 3:52 PM
 *
 */

package com.thirdsonsoftware;

import java.io.Serializable;
import java.security.InvalidParameterException;

/**
 * Represents a card in the game.
 *
 * @author Andrew B. Montcrieff
 * @author www.ThirdSonSoftware.com
 * @version 0.1
 * @since 0.0
 */
public class Card implements Comparable, Serializable {

    private char NONE = ' ';           // No player
    private int id;                    // Unique tile id

    final private int value;           // Value of the card
    final private Face face;           // A,2,3,4,5,6,7,8,9,10,J,Q,K
    final private Suit suit;           // Clubs, Diamonds, Hearts, Spades

    private Boolean played;            // Has this piece been placed?
    private Boolean hand;              // Is this piece in a hand?

    static protected boolean bUseColors = false;

    static String colors[] = new String[5];

    static String c = ""; // Color
    static String b = ""; // Color Blue
    static String r = ""; // Color Reset

    /**
     * Default constructor for a Card
     * @param id provides for a unique id for each card
     * @param face representation for the Face of a card
     * @param suit representation for the Suit of a card
     * @param value the value of the card
     */
    public Card(int id, Face face, Suit suit, int value) {

        // These properties are immutable
        this.id = id;
        this.face = face;
        this.suit = suit;
        this.value = value;

        colors[0] = Log.RED;
        colors[1] = Log.GREEN;
        colors[2] = Log.BLUE;
        colors[3] = Log.YELLOW;
        colors[4] = Log.WHITE;

        setInHand(false);
        setPlayed(false);

        setUseColor(false);
    }

    /**
     * Provides for the use of Color when drawing or displaying a Card
     * @param useColor will display use color or not
     */
    static protected void setUseColor(boolean useColor) {
        bUseColors = useColor;
        if (bUseColors) {
            c = Log.CYAN;
            b = Log.BLUE;
            r = Log.RESET;
        } else {
            c = b = r = "";
        }
    }

    /**
     * Set the unique id of the Card
     * @param id the value of the id field
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Returns back the unique id of a Card
     * @return an int representing the id of a Card
     */
    public int getId() {
        return id;
    }

    /**
     * Used to determine if a Card has been played or not
     * @return true if card has been played, false otherwise
     */
    public Boolean getPlayed() {
        return played;
    }

    /**
     * Provides the ability to mark a Card as played or not
     * @param played is the Card played
     */
    public void setPlayed(Boolean played) {
        this.played = played;
    }

    /**
     * Provides for the ability to mark a Card as still in hand or not
     * @return true if the Card is still in a Hand or not
     */
    public Boolean getInHand() {
        return hand;
    }

    /**
     * Provides the ability to mark a Card as still in hand or not
     * @param hand is the Card in hand
     */
    public void setInHand(Boolean hand) {
        this.hand = hand;
    }

    /**
     * What is the Face of this Card
     * @return Face of the Card
     */
    public Face getFace() { return face; }

    /**
     * What is the Suit of this Card
     * @return Suit of the Card
     */
    public Suit getSuit() { return suit; }

    /**
     * Retrieve a character that represents the Face of a Card
     * @return char representing the Face of a Card
     */
    public char getFaceChar() { return face.getCharacter(); }

    /**
     * Retrieve a character that represents the Suit of a Card
     * @return char representing the Suit of a Card
     */
    public char getSuitChar() { return suit.symbol().charAt(0); }

    /**
     * Retrive the value represented by the Face of a Card
     * @return int representing the value of the Face of the Card
     */
    public int getValue() {
        return getFace().getValue();
    }

    /**
     * Builds a textual representative of the Card including Face char, Suit char, Id, in hand and played indicators
     * @param row a String array that will hold the text representation of the Card.  Expected to have at least
     *            five(5) Strings in the array
     */
    public void displayCard(String[] row) {
        if ( row == null ) {
            throw (new InvalidParameterException("No string array provided to display the card."));
        } else if ( row[0] == null ) {
            throw (new InvalidParameterException("A string array with adequate space is required to display a card as a string."));
        } else if ( row.length < 5 ) {
            throw (new InvalidParameterException("A string array with at least 5 elements is required to display a card."));
        } else if ( row[1] == null || row[2] == null || row[3] == null || row[4] == null ) {
            throw (new InvalidParameterException("A string array with adequate space is required to display a card as a 5-line card."));
        } else {
            row[0] += String.format(c + " ----- "   + r);
            row[1] += String.format(c + "|%c%c   |" + r, getFaceChar(), getSuitChar());
            row[2] += String.format(c + "| (%2d)|"  + r, getId());
            row[3] += String.format(c + "|   %c%c|" + r, getSuitChar(), getFaceChar());
            row[4] += String.format(c + " --%c-- "  + r, getInHand() ? 'H' : (getPlayed() ? 'P' : '-'));
        }
    }

    @Override
    /**
     * Used to compare two cards to determine if they are the same
     * @return a value of -1 if less, 0 if the same, or 1 if greater
     */
    public int compareTo(Object o) {
        Card c = (Card) o;
        if (getFace().getValue() < c.getFace().getValue())
            return -1;
        else if (getFace().getValue() > c.getFace().getValue())
            return 1;
        return 0;
    }

    /**
     * Provides the unique generated hash for the Card, used for comparing, equality and sorting
     * @return int representing the hash for the Card
     */
    public int hashCode() {
        return toString().hashCode();
    }

    /**
     * Determines equality of two Cards
     * @param o the Card to compare this Card to
     * @return true if the same Card, false if not
     */
    public boolean equals(Object o) {
        if ((o instanceof Card) && ((Card)o).toString().equals(toString()))
            return true;
        return false;
    }

    /**
     * A string representation of the card.  This shows the four card
     * values in a single row, separated by a dash.
     * @return (String) a string representing the corner values of the tile
     */
    public String toString() {
        return String.format(c + "%c%s(%2d)-%2d" + r,face.getCharacter(),suit.symbol(),id,value);
    }
}
