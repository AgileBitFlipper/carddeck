/*
 *
 * Copyright (c) 2018 Third Son Software, LLC
 * (http://thirdsonsoftware.com/) and others. All rights reserved.
 * Created by Andrew B. Montcrieff on 1/9/18 6:34 PM
 *
 * This file is part of Project "carddeck".
 *
 * carddeck can not be copied and/or distributed without the express
 * permission of Andrew B. Montcrieff or Third Son Software, LLC.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Last modified: 1/7/18 11:41 PM
 *
 */

package com.thirdsonsoftware;

/**
 * Main class and method to show how a CardDeck can be used.
 */
public class Main {

    /**
     * The main entry point for this demonstration
     * @param args arguments for this demonstration (none are used)
     */
    public static void main(String[] args) {

        CardDeck deck = new CardDeck();

        deck.shuffleCards(1);

        System.out.println(deck.display(CardDeck.DISPLAY_AS_A_STRING));
        System.out.println(deck.display(CardDeck.DISPLAY_AS_A_CARD));
    }
}
