/*
 *
 * Copyright (c) 2018 Third Son Software, LLC
 * (http://thirdsonsoftware.com/) and others. All rights reserved.
 * Created by Andrew B. Montcrieff on 1/9/18 6:34 PM
 *
 * This file is part of Project "carddeck".
 *
 * carddeck can not be copied and/or distributed without the express
 * permission of Andrew B. Montcrieff or Third Son Software, LLC.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Last modified: 1/5/18 8:15 PM
 *
 */

package com.thirdsonsoftware;

/**
 * Each Card has a Face which defines Name, Value, and single-character representing that Face.
 */
public enum Face {

    ACE(1,'A'),
    TWO(2,'2'),
    THREE(3,'3'),
    FOUR(4,'4'),
    FIVE(5,'5'),
    SIX(6,'6'),
    SEVEN(7,'7'),
    EIGHT(8,'8'),
    NINE(9,'9'),
    TEN(10,'T'),
    JACK(10,'J'),
    QUEEN(10,'Q'),
    KING(10,'K');

    private int value;
    private char character;

    /**
     * Represents the Face of a Card, having both value and a single character representation.
     * @param value the value of the Face for a card
     * @param c the character that represents the Face of a card
     */
    private Face(int value, char c) {
        this.value = value ; 
        this.character = c ;
    }

    /**
     * Returns the value associated with a Card.
     * @return (int) value of the Face of a Card
     */
    public int getValue() {
        return this.value;
    }

    /**
     * Returns the single character that represents the Face of a Card.
     * @return (char) the character of the Face of a Card
     */
    public char getCharacter() { return this.character; }
}
