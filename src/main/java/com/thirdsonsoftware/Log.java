/*
 *
 * Copyright (c) 2018 Third Son Software, LLC
 * (http://thirdsonsoftware.com/) and others. All rights reserved.
 * Created by Andrew B. Montcrieff on 1/9/18 6:33 PM
 *
 * This file is part of Project "carddeck".
 *
 * carddeck can not be copied and/or distributed without the express
 * permission of Andrew B. Montcrieff or Third Son Software, LLC.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Last modified: 1/9/18 6:23 PM
 *
 */

package com.thirdsonsoftware;

/**
 *  Class for logging output.  Output colorized WHITE for information, RED for error, BLUE for debug, and YELLOW for
 *    warning.  Each output type has a method; Info() for information, Error() for error, Debug for debug, and
 *    Warning for warning messages.
 */
public class Log {

    public static boolean debugMode = false;

    // Foreground colors
    public static final String RESET = "\u001B[0m";
    public static final String BLACK = "\u001B[30m";
    public static final String RED = "\u001B[31m";
    public static final String GREEN = "\u001B[32m";
    public static final String YELLOW = "\u001B[33m";
    public static final String BLUE = "\u001B[34m";
    public static final String PURPLE = "\u001B[35m";
    public static final String CYAN = "\u001B[36m";
    public static final String WHITE = "\u001B[37m";

    // Background colors
    public static final String BLACK_BG = "\u001B[40m";
    public static final String RED_BG = "\u001B[41m";
    public static final String GREEN_BG = "\u001B[42m";
    public static final String YELLOW_BG = "\u001B[43m";
    public static final String BLUE_BG = "\u001B[44m";
    public static final String PURPLE_BG = "\u001B[45m";
    public static final String CYAN_BG = "\u001B[46m";
    public static final String WHITE_BG = "\u001B[47m";

    /**
     * This method retrieves the calling class for the log message.  It was intended to print this
     *   class name in the actual message along with a timestamp, however this made the logs extremely
     *   verbose.  For now, this method is not being called.
     * It will be left to implement this for each logging method, along with a timestamp, at a later
     *   date.
     * @param level the level at which to reflect back on
     * @return The calling Class
     * throws ClassNotFoundException
     */
    public static Class getCallerClass(int level) throws ClassNotFoundException {
        StackTraceElement[] stElements = Thread.currentThread().getStackTrace();
        String rawFQN = stElements[level + 1].toString().split("\\(")[0];
        return Class.forName(rawFQN.substring(0, rawFQN.lastIndexOf('.')));
    }

    /**
     * Turn debug mode on or off.
     *   When 'on', debug messages will be printed.
     *   When 'off', method call is essentially a No Op.
     * @param bModeEnabled true to enable debug logging, false otherwise.
     */
    public static void setDebugMode(boolean bModeEnabled) { debugMode = bModeEnabled ; }

    /**
     * Prints an information message to System.out
     *   Colorized output in WHITE.
     * @param message message string to display
     */
    public static void Info(String message) {
        System.out.println(WHITE + message + RESET);
    }

    /**
     * Prints an error message to System.out
     *   Colorized output in RED.
     * @param message message string to display
     */
    public static void Error(String message) {
        System.err.println(RED + message + RESET);
    }

    /**
     * Prints a debug message to System out
     *   Colorized output in BLUE.
     * @param message message string to display
     */
    public static void Debug(String message) {
        if (debugMode)
            System.out.println(BLUE + message + RESET);
    }

    /**
     * Prints a warning message to System.out
     *   Colorized output in YELLOW
     * @param message message string to display
     */
    public static void Warning(String message) {
        System.out.println(YELLOW + message + RESET);
    }

    // Todo: Add logging to a file support
}
