/*
 *
 * Copyright (c) 2018 Third Son Software, LLC
 * (http://thirdsonsoftware.com/) and others. All rights reserved.
 * Created by Andrew B. Montcrieff on 1/9/18 6:34 PM
 *
 * This file is part of Project "carddeck".
 *
 * carddeck can not be copied and/or distributed without the express
 * permission of Andrew B. Montcrieff or Third Son Software, LLC.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Last modified: 1/4/18 6:30 PM
 *
 */

package com.thirdsonsoftware;

/**
 * Each card in a deck of cards needs to be identified with a suit;
 *   Clubs, Diamonds, Hearts, or Spades
 *   Each suit has a name as well as a symbol.
 */
public enum Suit {

    CLUBS("Clubs", "\u2663"),
    DIAMONDS("Diamonds", "\u2666"),
    HEARTS("Hearts", "\u2665"),
    SPADES("Spades", "\u2660");

    private String suitName;
    private String suitSymbol;

    /**
     * Return the string name of the suit.
     * @return String name of the suit.
     */
    public String getSuitName() {
        return suitName;
    }

    /**
     * Return the string symbol representing the suit.
     * @return String suit symbol.
     */
    public String getSuitSymbol() {
        return suitSymbol;
    }

    /**
     * Constructor for a Suit, requiring both a name and a symbol
     * @param name The given name of the suit
     * @param symbol The given symbol for the suit
     */
    Suit(String name, String symbol) {
        this.suitName = name;
        this.suitSymbol = symbol;
    }

    /**
     * Returns the name of the suit; one of Clubs, Diamonds, Hearts, and Spades
     * @return A String containing the name of the Suit
     */
    public String suitName() {
        return suitName;
    }

    /**
     * Returns the symbol used to identify the suit; "♣", "♦", "♥", or "♠"
     * @return A String containing the symbol of the Suit
     */
    public String symbol() {
        return suitSymbol;
    }
}
